color = {}
function color.between(c1, c2, percentage)
  c1 = string.format('%08x', c1)
  c2 = string.format('%08x', c2)
  local result = ''
  for i = 1, 8 do
    if i % 2 == 1 then
      local n1 = tonumber(c1[i] .. c1[i + 1], 16) * (1 - percentage)
      local n2 = tonumber(c2[i] .. c2[i + 1], 16) * percentage
      local n = math.floor(n1 + n2)
      if n >= 256 then n = 255 end
      if n <= 0 then n = 0 end
      result = result .. string.format('%02x', n)
    end
  end
  local res = tonumber(result, 16)
  return res
end


function SkympButton(id, text, width, height)
  id = tostring(id); text = tostring(text)
  if width and type(width) ~= 'number' then error('SkympButton: argument 3 (width) must be number or nil') end
  if height and type(height) ~= 'number' then error('SkympButton: argument 4 (height) must be number or nil') end
  local v = imgui.cache[id] -- В cache будем хранить процент текущего цвета
  if v == nil then v = 0 end
  local c = color.between(0xff000000, 0xffffffff, v) -- Находим цвет между черным и белым на расстоянии v (от 0.0 до 1.0)
  local c2 = color.between(0xffffffff, 0xff000000, v)
  imgui.PushStyleColor(imgui.constant.Col.Button, c) -- Применяем цвет к кнопке, нажатой кнопке и наведённой кнопке
  imgui.PushStyleColor(imgui.constant.Col.ButtonActive, c)
  imgui.PushStyleColor(imgui.constant.Col.ButtonHovered, c)
  imgui.PushStyleColor(imgui.constant.Col.Text, c2) -- Применяем цвет к тексту
  local clicked = imgui.Button(text, width, height)
  for i = 1, 4 do imgui.PopStyleColor() end
  if (imgui.IsItemHovered()) and (v < 1) then imgui.cache[id] = v + 0.1 end -- Добавляем белый цвет кнопке и черный тексту
  if (not imgui.IsItemHovered()) and (v > 0) then imgui.cache[id] = v - 0.1 end -- Наоборот
  return clicked
end

function ClearSkympButton(id)
  pcall(function () imgui.cache[id] = nil end)
end

function SkympLink(id, text, color_, hoverColor, width, height)
  id = tostring(id); text = tostring(text)
  if width and type(width) ~= 'number' then error('SkympButton: argument 5 (width) must be number or nil') end
  if height and type(height) ~= 'number' then error('SkympButton: argument 6 (height) must be number or nil') end
  if color_ == nil then color_ = 0xffffffff end
  if hoverColor == nil then hoverColor = 0xffffcc33 end
  local v = imgui.cache[id]
  if v == nil then v = 0 end
  local c = color.between(color_, hoverColor, v)
  -- Ссылка - это невидимая кнопка с видимым текстом:
  imgui.PushStyleColor(imgui.constant.Col.Button, 0)
  imgui.PushStyleColor(imgui.constant.Col.ButtonActive, 0)
  imgui.PushStyleColor(imgui.constant.Col.ButtonHovered, 0)
  imgui.PushStyleColor(imgui.constant.Col.Text, c)
  local clicked = imgui.Button(text, width, height)
  for i = 1, 4 do imgui.PopStyleColor() end
  if (imgui.IsItemHovered()) and (v < 1) then
    imgui.cache[id] = v + 0.2
    if imgui.cache[id] >= 1 and imgui.IsCursorVisible() then imgui.PlaySound(0x3c759) end
  end
  if (not imgui.IsItemHovered()) and (v > 0) then
    imgui.cache[id] = v - 0.1
  end
  return clicked
end

-- Весь текст, который отрисовывается внутри функции f, будет skymp-шного шрифта указанной высоты
function WithSkympFont(f, fontHeight)
  if fontHeight == nil then fontHeight = 23 end
  if fontHeight < 23 then fontHeight = 23 end
  if fontHeight > 50 then fontHeight = 50 end
  imgui.PushSkympFont(fontHeight)
  local success, err = pcall(f)
  imgui.PopFont()
  if err then error(err) end
end

function InSkympWindow(id, flags, f)
  flags = flags | imgui.constant.WindowFlags.NoNav | imgui.constant.WindowFlags.NoCollapse | imgui.constant.WindowFlags.NoTitleBar
  if imgui.IsCursorVisible() == false then
    flags = flags | imgui.constant.WindowFlags.NoMove -- Если курсор невидим, запрещаем двигать им окно
  end
  imgui.Begin(id, flags)
  local success, err = pcall(f)
  imgui.End()
  if err then error(err) end
end

function SpriteButton(id, path, width, height, hovWidth, hovHeight)
  if type(id) ~= 'string' then error('SpriteButton: argument 1 (id) must be string') end
  if type(path) ~= 'string' then error('SpriteButton: argument 2 (path) must be string') end
  if type(width) ~= 'number' then error('SpriteButton: argument 3 (width) must be number') end
  if type(height) ~= 'number' then error('SpriteButton: argument 4 (height) must be number') end
  if type(hovWidth) ~= 'number' then error('SpriteButton: argument 5 (hovWidth) must be number') end
  if type(hovHeight) ~= 'number' then error('SpriteButton: argument 6 (hovHeight) must be number') end
  if imgui.cache.spriteButton == nil then imgui.cache.spriteButton = {} end
  if imgui.cache.spriteButton[id] == nil then imgui.cache.spriteButton[id] = {} end
  if imgui.cache.spriteButton[id].percentage == nil then imgui.cache.spriteButton[id].percentage = 0 end
  local v = imgui.cache.spriteButton[id].percentage

  imgui.PushStyleColor(imgui.constant.Col.Border, -1)
  imgui.BeginChild('SpriteButton' .. id .. '_Child', hovWidth, hovHeight, imgui.constant.WindowFlags.NoScrollBar)
  local w, h = width + (hovWidth - width) * v, height + (hovHeight - height) * v
  imgui.SetCursorPos(0.5 * (hovWidth - w), 0.5 * (hovHeight - h))
  imgui.Image(path, w, h)
  local clicked = imgui.IsItemClicked()
  local hovered = imgui.IsItemHovered()
  imgui.EndChild()
  imgui.PopStyleColor()
  if hovered then
    v = v + 0.1
    if v > 1 then v = 1 end
  else
    v = v - 0.1
    if v < 0 then v = 0 end
  end
  imgui.cache.spriteButton[id].percentage = v
  return clicked
end

function SetSpriteButtonPercentage(id, newPercentage)
  if type(newPercentage) ~= 'number' then newPercentage = 0
  elseif newPercentage > 1 then newPercentage = 1
  elseif newPercentage < 0 then newPercentage = 0 end
  pcall(function () imgui.cache.spriteButton[id].percentage = newPercentage end)
end

function SkympDialog(text, buttons)
  local clickedIndex = nil
  imgui.SetCursorVisible(1)
  InSkympWindow('###skympDialog', imgui.constant.WindowFlags.NoResize | imgui.constant.WindowFlags.NoMove, function ()
    local textW, textH = imgui.CalcTextSize(text)
    local w, h = textW, textH
    w = w + textH * 2
    h = h + textH * 5
    imgui.SetWindowSize(w, h)
    imgui.SetWindowPos(imgui.GetDisplayWidth() / 2 - w / 2, imgui.GetDisplayHeight() / 2 - h / 2)
    imgui.SetCursorPos(w / 2 - textW / 2, 0.5 *(h / 2 - textH / 2))
    imgui.Text(text)
    if buttons then
      local btnW = 150
      local btnH = 37.5
      local gap = 30
      for i = 1, #buttons do
        local placeForOneBtn = w / #buttons
        local x = w * ((i - 1) / #buttons)
        x = x + placeForOneBtn / 2 - btnW / 2
        imgui.SetCursorPos(x, h - btnH - gap)
        if SkympButton('###SkympDialogButton' .. i, buttons[i], btnW, btnH) then
          clickedIndex = i
          imgui.PlaySound(0x3c751)
        end
        imgui.SameLine()
      end
    end
  end)
  return clickedIndex
end

function ClearSkympDialog()
  for i = 1, 10 do ClearSkympButton('###SkympDialogButton' .. i) end
end
