-- http://lua-users.org/wiki/StringIndexing
-- String indexing
getmetatable('').__index = function (str,i)
  return string.sub(str,i,i)
end
getmetatable('').__call = function(str,i,j)
  if type(i)~='table' then return string.sub(str,i,j)
  else
    local t={}
    for k,v in ipairs(i) do t[k]=string.sub(str,v,v) end
    return table.concat(t)
  end
end

--[[
  lua_imgui_bindings Patches
]]--

imgui.cache = {}

-- Allows width and height to be nil
local g_Button = imgui.Button
imgui.Button = function (text, width, height)
  if width == nil or height == nil then return g_Button(text) else return g_Button(text, width, height) end
end

local g_Begin2 = imgui.Begin2
imgui.Begin = function (id, flags)
  if flags == nil then flags = 0 end
  g_Begin2(id, flags)
end
imgui.Begin2 = nil

-- imgui.Text is missing in bindings
imgui.Text = function (format, arg1, arg2, arg3, arg4, arg5, arg6, arg7)
  format = tostring(format)
  local text = format
  pcall(function ()
    if arg1 == nil then
    elseif arg2 == nil then text = string.format(format, arg1)
    elseif arg3 == nil then text = string.format(format, arg1, arg2)
    elseif arg4 == nil then text = string.format(format, arg1, arg2, arg3)
    elseif arg5 == nil then text = string.format(format, arg1, arg2, arg3, arg4)
    elseif arg6 == nil then text = string.format(format, arg1, arg2, arg3, arg4, arg5)
    elseif arg7 == nil then text = string.format(format, arg1, arg2, arg3, arg4, arg5, arg6)
    else text = string.format(format, arg1, arg2, arg3, arg4, arg5, arg6, arg7) end
  end)
  imgui.TextUnformatted(text)
end

imgui.SetStyleColor = function (var, val, f)
  if var and val and f then
    imgui.PushStyleColor(var, val)
    local suc, err = pcall(f)
    imgui.PopStyleColor(1)
    if not suc then error(err) end
  end
end

imgui.Child = function (id, width, height, f, flags)
  if not flags then flags = 0 end
  imgui.BeginChild(id, width, height, true, flags)
  if type(f) == 'function' then
    local suc, err = pcall(f)
    if not suc then
      imgui.EndChild()
      error(err)
    end
  end
  imgui.EndChild()
end
