local menuError = {}

function menuError.InSkympDialog(id, f, options)
  local validXy = options ~= nil and options.x ~= nil and options.y ~= nil
  local validSize = options ~= nil and options.width ~= nil and options.height ~= nil
  local flags = imgui.constant.WindowFlags.NoResize
  if validXy then
    flags = flags | imgui.constant.WindowFlags.NoMove
  end
  InSkympWindow(id, flags, function()
    if validXy then
      imgui.SetWindowPos(options.x, options.y)
    end
    if validSize then
      imgui.SetWindowSize(options.width, options.height)
    end
    f()
  end)
end

function menuError.setError(err)
  menuError.err = err
end

menuError.on = {
  render = function ()
    if menuError.err then
      local txt = tostring(menuError.err)
      menuError.InSkympDialog('menuErrorDialog', function () imgui.Text(txt) end, { x = 15, y = 15, width = imgui.CalcTextSize(txt) + 20, height = 36})
    end
  end
}

return menuError
