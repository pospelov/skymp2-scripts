local example = {}

example.on = {
  render = function ()
  end,

  interactionClick = function (targetName, index)
  end,

  broadcastIn = function (contentType, content)
  end

}

return example
