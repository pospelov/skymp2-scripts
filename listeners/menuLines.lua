-- Рисует линии за бегущими игроками
-- Создано для отладки
-- Открывается на F10. Всем советую посмотреть :)

local menuLines = {}
local CONTENT_MOVEMENT = '2'

function menuLines.reset()
  menuLines.show = false
  menuLines.pos = {}
  menuLines.text = {}
  menuLines.prevPos = {}
end
menuLines.reset()

function menuLines.renderOne(text, key)
  local f = function ()
    text = tostring(text)
    local w, h = imgui.CalcTextSize(text)
    h = h + 20
    w = w + 20
    imgui.SetWindowSize(imgui.GetDisplayWidth(), imgui.GetDisplayHeight())
    imgui.SetWindowPos(0, 0)
    local cx, cy = imgui.GetCursorPos()
    if not menuLines.pos[key] then menuLines.pos[key] = {-10000,-10000} end
    imgui.SetCursorPos(menuLines.pos[key][1], menuLines.pos[key][2])

    local tokens = {}
    local tok = ''
    for i = 1, #text do
      if text[i] == ' ' then
        table.insert(tokens, tok)
        tok = ''
      else
        tok = tok .. text[i]
      end
    end
    if tok ~= '' then table.insert(tokens, tok) end

    for i = 1, #tokens do
      tokens[i] = tonumber(tokens[i])
      imgui.Text(tokens[i])
      imgui.SameLine()
    end
    if tokens[1] and tokens[2] and tokens[3] then
      if not menuLines.prevPos[key] then menuLines.prevPos[key] = {} end
      table.insert(menuLines.prevPos[key], tokens)
      menuLines.pos[key] = imgui.WorldPointToScreenPoint(tokens[1], tokens[2], tokens[3])
      --imgui.Text(#menuLines.prevPos[key])
      local totalLength = 0
      for i = 3, #menuLines.prevPos[key] do
        local from3d = menuLines.prevPos[key][i - 1]
        local to3d = menuLines.prevPos[key][i]
        local sqr = function (x) return x * x end
        local p = math.sqrt(sqr(from3d[1] - to3d[1]) + sqr(from3d[2] - to3d[2]) + sqr(from3d[3] - to3d[3]))
        totalLength = totalLength + p
        local from = imgui.WorldPointToScreenPoint(from3d[1], from3d[2], from3d[3])
        local to = imgui.WorldPointToScreenPoint(to3d[1], to3d[2], to3d[3])
        if from[1] < 0 or from[2] < 0 then
        else
          imgui.DrawList_AddLine(from[1], from[2], to[1], to[2], 0xFF0000FF, 2)
          imgui.DrawList_AddLine(from[1], from[2], from[1], from[2] + 1, -1, 20)
        end
      end
      imgui.Text(totalLength)
    end
  end
  imgui.SetStyleColor(imgui.constant.Col.WindowBg, 0, function ()
    InSkympWindow('###menuLinesWindow' .. key, imgui.constant.WindowFlags.NoMove | imgui.constant.WindowFlags.NoResize | imgui.constant.WindowFlags.NoScrollbar, f)
  end)
end

menuLines.on = {
  render = function ()
    if imgui.IsKeyReleased(0x79) then
      menuLines.show = not menuLines.show
      if not menuLines.show then menuLines.reset() end
    end -- f10
    if not menuLines.show then return end
    for key, val in pairs(menuLines.text) do
      menuLines.renderOne(val, key)
    end
  end,

  interactionClick = function (targetName, index)
  end,

  broadcastIn = function (contentType, content, owner)
    if tonumber(owner) < 0xff000000 then return end
    if not menuLines.show then return end
    if contentType == CONTENT_MOVEMENT then
      menuLines.text[owner] = content
    end
  end

}

return menuLines
