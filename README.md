# ![](https://pp.userapi.com/c837339/v837339766/6a2be/wYK7LEvTyT8.jpg)

# Первые шаги

1) Создайте файл `scripts_path.txt` в папке `Skyrim` и впишите туда абсолютный путь до репозитория `skymp2-scripts`, например, `C:/skymp2-scripts`
2) Скопируйте `listeners\example.lua`
3) Напишите Hello World в событии `render`:
```lua
...
example.on = {
  render = function ()
    imgui.Text('Hello World')
  end,
...
```
4) В игре вы должны увидеть окно с заголовком Debug и надписью Hello World.

# Cheatsheet

```lua
imgui.Text('Hello Skymp') -- Текст
imgui.Text('%s %d %f', 'abc', 108, 1.33) -- C-Style Форматирование текста поддерживается


imgui.SetWindowPos(100, 100) -- Переместить окно на {100,100} ({0,0} в левом верхнем углу)
imgui.SetWindowSize(1000, 1000) -- Изменить размер окна на {1000, 1000}
imgui.GetDisplayWidth(); imgui.GetDisplayHeight() -- Получить ширину/высоту монитора


imgui.PlaySound(sound.UIMagicSelect) -- Проиграть звук
-- ID звуков:
-- UIMagicSelect, UIMagicUnselect, UIMapRollover, UIMapRolloverFlyout,
-- UISelectOff, UISelectOn, UIFavorite, UIActivateFail


-- Слайдер от 1 до 10 с текстом bla-bla
value = 1 -- Здесь будет результат выбора пользователя (число от 1 до 10)
local isClicked, value = imgui.SliderInt('bla-bla', value, 1, 10, "%.0f")


imgui.Text('I am CLICKABLE')
local clicked = imgui.IsItemClicked() -- true, если предыдущий элемент (текст) был кликнут
imgui.IsItemHovered() -- true, если пред. элемент был наведён


imgui.Text('Foo')
imgui.SameLine() -- Рендерить след. элемент на той же строке
imgui.Text('Bar')
-- Слова Foo Bar будут на одной строчке через пробел
-- Это полезно, если нужно на одной строке, к примеру, текст и кнопку


imgui.NewLine() -- Новая строка


-- Возможностей NewLine нам может не хватить
-- Вот так можно продолжить рисовать ниже и правее на 10 пикселей
-- Cursor - это не курсор, это место, с которого начнёт отрисовку след. элемент
local x, y = imgui.GetCursorPos()
imgui.SetCursorPos(x + 10, y + 10)


local isVisible = imgui.IsCursorVisible() -- Виден ли курсор мыши
imgui.SetCursorVisible(true) -- Сделать курсор мыши видимым/невидимым
-- С невидимым курсором все равно можно кликать на кнопки и т.д.
-- Помните это и каждый раз проверяйте, виден ли курсор


-- Найти ширину и высоту текста в пикселях можно так:
local width, height = imgui.CalcTextSize('Some Text')


local enter = imgui.IsKeyDown(0x0d) -- Нажата ли клавиша Enter в данный момент
-- ID клавиш тут:
-- https://docs.microsoft.com/en-us/windows/desktop/inputdev/virtual-key-codes
 imgui.IsKeyReleased(0x0d) -- Отпущена
 imgui.IsKeyPressed(0x0d) -- Нажата (возвращает true только один раз за каждое нажатие)


local isClicked = imgui.Button('click me', 100, 200) -- Отрисовать кнопку с шириной 100 и высотой 200
local isClicked2 = SkympButton('###blablaId', 'click me', 100, 200)
-- То же самое, только кнопка в стиле SkyMP.
-- Чаще всего нужна именно такая. ###blablaId - это уникальный ID.
-- Уникальные ID нужно придумывать самим. Главное, чтобы начинались с ###
-- И не было разных элементов с одинаковыми ID
-- SkympButton пишется без imgui, т.к. это не часть imgui


imgui.ClearSkympButton('###blablaId') -- Сбросит анимацию skymp-кнопки с таким ID
-- Вызывайте это, перед тем как перестать отрисовывать кнопку
-- Это нужно, чтобы в следующий раз, когда это меню откроют, цвет кнопки был корректен


local white, blue = 0xffffffff, 0xffffcc33
local isClickedLink = SkympLink('###skympLinkUniqueId', 'Я - голубая при наведении ссылка', white, blue, 100, 50)
-- 100x50 - это ширина и высота, указывать не обязательно


local path = 'button.png' -- Картинка из папки assets
local width, height, hovWidth, hovHeight = 32, 32, 64, 64 -- Кнопка будет увеличиваться с 32x32 до 64x64 при наведении мыши
SpriteButton("###spriteButtonId", path, width, height, hovWidth, hovHeight)


imgui.DisableMenu('interactionMenu', 100) -- Откл. меню взаимодействия на 0.1сек
imgui.DisableMenu('escapeMenu', 100) -- Откл. escape-меню на 0.1сек


local color = 0xffffcc33 -- Цвет в формате ABGR (как RGBA только наоборот)
imgui.SetStyleColor(imgui.constant.Col.Text, color, function ()
    imgui.Text('Текст голубого цвета')
 end)
 -- Также из полезного есть цвет рамки imgui.constant.Col.Border


imgui.Image('some.png') -- Отрисовать изображение. Должно быть в папке assets


local childWidth, childHeight = 200, 100
imgui.Child('###childUniqueId', childWidth, childHeight, function ()
	-- Child - это что-то вроде ячейки в таблице html
	-- "Подокно" внутри окна с собственными координатами курсора (Get/SetCursorPos)
end)


WithSkympFont(function ()
	imgui.Text('Этот текст будет шрифтом скаймп 23 размера')
end, 23)

-- Смешивание цветов в определённой пропорции:
local white = 0xffffffff, black = 0xff000000
local grey = color.between(white, black, 0.5)
local lightGrey = color.between(white, black, 0.25)

```

# Пример окна
```lua
-- Окно с текстом "Hello" размером 800x600, всегда по центру экрана
-- Следующий код должен быть в on.render
local winWidth, winHeight = 800, 600
local width, height = imgui.GetDisplayWidth(), imgui.GetDisplayHeight()
local showWindow = true
if showWindow then
	InSkympWindow('###MyWindowId', imgui.constant.WindowFlags.NoMove | imgui.constant.WindowFlags.NoResize, function ()
	    imgui.SetWindowPos(0.5 * (width - winWidth), 0.5 * (height - winHeight))
	    imgui.SetWindowSize(winWidth, winHeight)
	    imgui.Text('Hello')
	end)
end
```

# Ссылки
[Learn Lua in 15 Minutes](http://tylerneylon.com/a/learn-lua/)
[LuaStyleGuide](http://lua-users.org/wiki/LuaStyleGuide)

# TODO
 - [ ] API полей ввода
 - [ ] API система взаимодействия
 - [ ] Сетевое API
